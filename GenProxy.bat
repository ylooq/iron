@rem # ����Proxy
@rem #--------------------------------------------------------------------------------------
@rem # param1: package
@rem # param2: target dir
@echo off




call env.bat


@rem # gamesrv
java -cp ./gamesrv/%CLASS%;./common/%CLASS%;./core/%CLASS%;./libs/*;./config/ cn.pink.core.gen.proxy.GenProxy cn.pink.gamesrv /gamesrv/src/gen/java/
java -cp ./logicsrv/%CLASS%;./common/%CLASS%;./core/%CLASS%;./libs/*;./config/ cn.pink.core.gen.proxy.GenProxy cn.pink.logicsrv /logicsrv/src/gen/java/
java -cp ./gatesrv/%CLASS%;./common/%CLASS%;./core/%CLASS%;./libs/*;./config/ cn.pink.core.gen.proxy.GenProxy cn.pink.gatesrv /gatesrv/src/gen/java/
java -cp ./centersrv/%CLASS%;./common/%CLASS%;./core/%CLASS%;./libs/*;./config/ cn.pink.core.gen.proxy.GenProxy cn.pink.centersrv /centersrv/src/gen/java/


@if "%1" == "" pause