package cn.pink.core.interfaces;

/**
 * 线程接口
 * @Author: pink
 * @Date: 2022/6/16 15:30
 */
public interface IThreadCase {
    /**
     * 线程启动时执行的操作
     */
    void caseStart();

    /**
     * 线程结束时执行的操作
     */
    void caseStop();

    /**
     * 线程每帧执行的操作
     * @param delay delay
     */
    void caseRunOnce(int delay);
}
