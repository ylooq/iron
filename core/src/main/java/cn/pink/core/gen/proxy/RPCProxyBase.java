package cn.pink.core.gen.proxy;

import cn.pink.core.support.Param;
import cn.pink.core.support.function.IronFunction2;
import cn.pink.core.support.function.IronFunction3;

/**
 * RPC代理类的基类
 * @author Pink
 */
public abstract class RPCProxyBase {
	/**
	 * 异步注册监听RPC返回
	 */
	public abstract void listenResult(IronFunction2<Param, Param> method, Object...context);
	public abstract void listenResult(IronFunction3<Boolean, Param, Param> method, Object...context);
}
