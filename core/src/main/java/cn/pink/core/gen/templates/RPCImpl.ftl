package ${packageName};
                    
import cn.pink.core.Service;
import cn.pink.core.gen.proxy.RPCImplBase;
import ${rootPackageName}.RPCProxy.${className}Proxy.EnumCall;
import cn.pink.core.support.function.*;
<#if importPackages??>
<#list importPackages as package>
import ${package};
</#list>
</#if>
import cn.pink.core.gen.IronGenFile;

${mainAnnotation}
@IronGenFile
public final class ${className}Impl extends RPCImplBase {
	
	/**
	 * 获取函数指针
	 */
	@Override	
	public Object getMethodFunction(Service service, int methodKey) {
		${className} serv = (${className})service;
		switch (methodKey) {
			<#list methods as method>
			case EnumCall.${method.enumCall}: {
				<#if method.hasException>
				IronFunction${method.paramsSize}${method.functionTypes} f = (${method.paramsCall}) -> { try { serv.${method.name}(${method.paramsCall}); } catch(Exception e) { throw new cn.pink.core.support.SysException(e); } };
				return f;
				<#else>
				return (IronFunction${method.paramsSize}${method.functionTypes})serv::${method.name};
				</#if>
			}
			</#list>
			default: break;
		}
		
		return null;
	}

}
