package cn.pink.core.gen.proxy;

import cn.pink.core.NodeType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 生成Proxy代理接口类需用设置此注解
 * @author Pink
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface DistrClass {
	NodeType nodeType() default NodeType.NONE;

	boolean singleton() default true;
	
	/** 生成的Proxy代理类中需要额外引用的类 */
	Class<?>[] importClass() default {};
}