package cn.pink.core.gen.proxy;

import cn.pink.core.Service;

/**
 * RPC实现类的基类
 * @author Pink
 */
public abstract class RPCImplBase {	
	/**
	 * 根据函数id获取service上的RPC函数
	 * @param serv serv
	 * @param methodKey methodKey
	 * @return t
	 */
	public abstract <T> T getMethodFunction(Service serv, int methodKey);
}
