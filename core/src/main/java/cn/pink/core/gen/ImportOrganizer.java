package cn.pink.core.gen;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 为自动生成工具提供整理import包的功能
 * @author Pink
 */
public class ImportOrganizer {

	/** 模板中预先导入的包 */
	protected Set<String> preConfiguredImports = new HashSet<String>();

	public ImportOrganizer() {}
	
	public static ImportOrganizer newInstance() {
		return new ImportOrganizer();
	}
	
	/**
	 * 增加已在模板中预先导入的包<br>
	 * 与{@link organizeImports}配合使用
	 * @return this 可以链式调用
	 */
	public ImportOrganizer appendPreConfiguredImports(String packageName) {
		this.preConfiguredImports.add(packageName);
		return this;
	}
	
	/**
	 * 组织需要导入的包
	 * 
	 */
	public List<String> organizeImports(List<String> packageListIn) {
		// 去重
		Set<String> temp = new HashSet<String>(packageListIn);
		// 组织
		return organizeImports(temp);
	}
	
	/**
	 * 组织需要导入的包
	 * 
	 */
	public List<String> organizeImports(Set<String> packageSetIn) {		
		// 转成List。。。
		List<String> ret = new ArrayList<String>(packageSetIn);
		// 去掉已在模板中预先导入的包
		ret.removeAll(preConfiguredImports);
		// 排序
		ret.sort(null);
		return ret;
	}
}
