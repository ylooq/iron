package cn.pink.core.gen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自动生成工具的基类
 * @author Pink
 */
public abstract class GenBase {
	/** 插件指定生成的文件名 */
	public static String pluginDesFileName;

	/** 用户当前执行的所在路径 */
	public static String BASE_DIR = GenBase.class.getResource("/").getPath();
	
	/** 模板文件路径 */
	public static final String TEMP_DIR = "/cn/pink/core/gen/templates/";
	
	/** 文件夹相关 */
	/** 配置源文件夹 */
	protected String packageName;
	/** 输出目标文件夹 */
	protected String targetDir;

	/** 文件生成相关 */
	/** 要放到模板的信息 */
	protected List<Map<String, Object>> rootMaps = new ArrayList<>();
	/** 是否能正常生成 */
	public boolean canGen;
	
	public GenBase(String packageName, String targetDir) {
		this.packageName = packageName;
		this.targetDir = targetDir;
	}
	
	/**
	 * 自动生成文件的具体方法
	 */
	public void genFiles() throws Exception {
		// 判断能否生成
		if (!isCanGen()) {
			System.out.println("代码生成失败，请检查错误后重试！");
		} else {
			// 生成单一的全局代码，如ListenerInit、MsgReceiverInit
			genGlobalFile();
			
			// 遍历每个文件，生成对应的辅助代码
			for (Map<String, Object> rootMap : rootMaps) {
				genFileHandler(rootMap);
			}
		}
	}
	
	/**
	 * 对该类注解生成全局唯一的文件，子类可覆盖
	 */
	private void genGlobalFile() throws Exception {
		// 判断是否为空
//		if (this.rootMaps.isEmpty()) {
//			return;
//		}
		
		Map<String, Object> data = new HashMap<>();
		data.put("rootPackageName", this.packageName);
		data.put("methodsList", this.rootMaps);
		
		genGlobalFileHandler(data);
	}
	
	protected void genGlobalFileHandler(Map<String, Object> rootMaps) throws Exception {
		
	}
	
	/**
	 * 生成类的核心方法，各子类实现
	 */
	protected abstract void genFileHandler(Map<String, Object> rootMap) throws Exception;
	
	/**
	 * 判断能否成功生成代码
	 */
	public boolean isCanGen() {
		return canGen;
	}
	
}
