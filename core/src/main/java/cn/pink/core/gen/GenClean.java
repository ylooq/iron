package cn.pink.core.gen;

import cn.pink.core.support.ClassFinder;

import java.io.File;
import java.util.List;

/**
 * @author Pink
 */
public class GenClean {
	/** 待删除文件夹 */
	private String targetDir;
	/** 待删除最外层包名 */
	private String packageName;
	
	public GenClean(String pack, String targetDir) {
		this.packageName = pack;
		this.targetDir = targetDir;
	}
	
	/**
	 * 清楚目录下的有GenCallbackFile，GenProxyFile, GenEntityFile注解的类
	 */
	public void clean() {
		// 获取待删除文件夹下的所有类
		List<Class<?>> sources = ClassFinder.getAllClass(packageName);
		
		// 遍历所有类，删除有注解的生成实体类
		for (Class<?> clazz : sources) {
			// 删除有JowGenFile注解的类
			if (clazz.isAnnotationPresent(IronGenFile.class)) {
				String filePath = targetDir + ClassFinder.packageToPath(clazz.getPackage().getName()) + "/" + clazz.getSimpleName() + ".java";
				
				delete(filePath);
			}
		}
	}
	
	/**
	 * 删除文件
	 * @param filePath
	 */
	public void delete(String filePath) {
		File file = new File(filePath);
		if (file.isFile() && file.exists()) {
			file.delete();
			System.out.println("删除文件：" + filePath);
		}
	}
	
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Usage GenClean packName targetDir");
			return;
		}
		
		// 设置log4j日志文件名
		System.setProperty("logFileName", "GenClean");
		
		String packName = args[0];
		String targetDir = System.getProperty("user.dir") + args[1];
		
		GenClean genClean = new GenClean(packName, targetDir);
		genClean.clean();
	}

}
