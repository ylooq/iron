package cn.pink.core;

/**
 * 节点类型
 * @Author: pink
 * @Date: 2022/6/13 16:15
 */
public enum NodeType {
    /** 网关 */
    GATE,

    /** 游戏 */
    GAME,

    /** 逻辑 */
    LOGIC,

    /** 中心 */
    CENTER,

    /** 啥也不是, 直接报错 */
    NONE
}
