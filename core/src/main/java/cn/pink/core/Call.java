package cn.pink.core;

import cn.pink.core.support.Param;

import java.io.Serializable;

/**
 * 节点传输对象
 * @Author: pink
 * @Date: 2022/6/7 11:11
 */
public class Call implements Serializable {
    /** 请求类型 远程调用 */
    public static final int TYPE_RPC = 1000;
    /** 请求类型 远程调用返回 */
    public static final int TYPE_RPC_RETURN = 2000;

    /** 请求ID */
    public long id;
    /** 请求类型 1000 2000 3000 4000 */
    public int type;
    /** 发送方NodeId */
    public String fromNodeId;
    /** 发送方PortId(Call请求听过port发送，简化业务没精确到service) */
    public String fromPortId;
    /** 接收方 */
    public CallPoint to = new CallPoint();
    /** 调用函数名称 */
    public int methodKey;
    /** 调用函数参数 */
    public Object[] methodParam;
    /** 返回值 */
    public Param returns = new Param();
    /** 扩展参数 */
    public Param param = new Param();

    /**
     * 创建CallReturn
     */
    public CallReturn createCallReturn() {
        return new CallReturn(id, fromNodeId, fromPortId);
    }
}
