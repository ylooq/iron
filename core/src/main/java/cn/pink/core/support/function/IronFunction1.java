package cn.pink.core.support.function;

/**
 * 接受一个参数的函数
 * @author Pink
 * @param <T1>
 */
@FunctionalInterface
public interface IronFunction1<T1> {
	
	void apply(T1 t1);

}
