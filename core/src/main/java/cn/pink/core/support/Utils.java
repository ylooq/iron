package cn.pink.core.support;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Locale;

/**
 * 工具类，各种乱七八糟的接口
 * @author Pink
 */
public class Utils {
	/**
	 * String TO int<br>
	 * 如果出错 则为0
	 */
	public static int intValue(String value) {
		if (StrUtil.isNotEmpty(value) && NumberUtil.isNumber(value)) {
			return Double.valueOf(value).intValue();
		} else {
			System.err.println("数据错误:" + value);
			return 0;
		}
	}

	/**
	 * 获取rpc调用处的函数信息，文件名:行号 调用者函数名
	 */
	public static String getCallerInfo() {
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		StackTraceElement e = stackTrace[3];

//		System.out.println("methed name: " + e.getFileName() + " - " + e.getLineNumber() + " - " + e.getMethodName());

		return new StringBuilder().append(e.getFileName()).append(":")
				.append(e.getLineNumber()).append(" ")
				.append(e.getMethodName()).toString();
	}

	/**
	 * FreeMarker生成代码
	 * @param pathPrefix 模板文件所在目录
	 * @param tempFile 模板文件的名字
	 * @param rootMap 要带入模板的变量
	 * @param targetDir 生成文件的目标目录
	 * @param targetFile 生成目标文件的名字
	 */
	public static void freeMarker(String pathPrefix, String tempFile, Object rootMap,
								  String targetDir, String targetFile) throws Exception {
		Configuration configuration = new Configuration();
		configuration.setClassForTemplateLoading(Utils.class, pathPrefix);
		configuration.setEncoding(Locale.getDefault(), "UTF-8");
		Template temp = configuration.getTemplate(tempFile, "UTF-8");

		// 判断目标文件夹不存在 ，则新建文件夹
		File dir = new File(targetDir);
		if (!dir.exists()) {
			dir.mkdirs();
		}

		// 目标文件名(包含路径的名称)
		String fileFullName = targetDir + targetFile;
		System.out.println("---------开始生成" + fileFullName + "文件......---------");

		// 根据模版生成文件
		File target = new File(fileFullName);
		Writer out = new OutputStreamWriter(new FileOutputStream(target), "UTF-8");
		temp.process(rootMap, out);
		out.flush();
		out.close();

		System.out.println("---------" + targetFile + "文件生成完毕！---------\n");
	}
}
