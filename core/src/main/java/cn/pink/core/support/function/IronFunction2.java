package cn.pink.core.support.function;

/**
 * 接受2个参数的函数
 * @author Pink
 * @param <T1>
 * @param <T2>
 */
@FunctionalInterface
public interface IronFunction2<T1, T2> {
	
	void apply(T1 t1, T2 t2);

}
