package cn.pink.core.support.wrap;

public class VObj<T> {
    public T value;

    public VObj() {

    }

    public VObj(T v) {
        value = v;
    }
}
