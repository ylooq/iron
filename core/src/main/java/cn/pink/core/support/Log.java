package cn.pink.core.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * log
 * @Author: pink
 * @Date: 2022/6/10 20:42
 */
public class Log {
    public static Logger error = LoggerFactory.getLogger("ERROR");

    public static Logger common = LoggerFactory.getLogger("COMMON");

    public static Logger system = LoggerFactory.getLogger("SYSTEM");

    public static Logger gate = LoggerFactory.getLogger("GATE");
    public static Logger game = LoggerFactory.getLogger("GAME");
    public static Logger center = LoggerFactory.getLogger("CENTER");
    public static Logger logic = LoggerFactory.getLogger("LOGIC");

    public static Logger effect = LoggerFactory.getLogger("COMMON_EFFECT");
    public static Logger stat = LoggerFactory.getLogger("COMMON_STAT");
}
