package cn.pink.core.support.function;

/**
 * 0个参数的函数
 * @author Pink
 */
@FunctionalInterface
public interface IronFunction0 {
	
	void apply();

}
