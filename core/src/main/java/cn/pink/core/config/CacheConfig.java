package cn.pink.core.config;

/**
 * 缓存键配置
 * @Author: pink
 * @Date: 2022/6/10 21:42
 */
public enum CacheConfig {
    /** node */
    NODE("node", 0, 0),
    ;

    private final String key;
    private final long size;
    private final long ttl;

    CacheConfig(String key, long size, long ttl) {
        this.key = key;
        this.size = size;
        this.ttl = ttl;
    }

    public String getKey() {
        return key;
    }

    public long getSize() {
        return size;
    }

    public long getTtl() {
        return ttl;
    }
}
