package cn.pink.core.config;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.json.JsonObject;

/**
 * @Author: pink
 * @Date: 2022/5/20 11:20
 */
public class NodeDeployConfig {


    public static DeploymentOptions getCenterOpt(int index, int instance) {
        return new DeploymentOptions().setConfig(new JsonObject().put("nodeId", "center" + index)).setInstances(1);
    }

    public static DeploymentOptions getGameOpt(int index, int instance) {
        return new DeploymentOptions().setConfig(new JsonObject().put("nodeId", "game" + index)).setInstances(instance);
    }

    public static DeploymentOptions getLogicOpt(int index, int instance) {
        return new DeploymentOptions().setConfig(new JsonObject().put("nodeId", "logic" + index)).setInstances(instance);
    }

    public static DeploymentOptions getGateOpt(int index, int instance) {
        return new DeploymentOptions().setConfig(new JsonObject().put("nodeId", "gate" + index)).setInstances(instance);
    }
}
