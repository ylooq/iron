package cn.pink.core.config;

import cn.hutool.setting.dialect.Props;

/**
 * Iron全局配置
 * @Author: pink
 * @Date: 2022/5/18 14:42
 */
public class IronConfig {
    /** 配置文件路径 */
    public static String CONFIG_PATH = System.getProperty("user.dir") + "/config/";
    /** 配置文件名称 */
    private static final String CONFIG_NAME = "iron.properties";

    public static String COMMON_GAME;
    public static boolean COMMON_DEBUG;
    public static int COMMON_FPS;
    public static DeployConfig COMMON_DEPLOY;


    public static String CLUSTER_HOST;
    public static String CLUSTER_MEMBER;


    public static Boolean STAT_ENABLE;
    public static Integer STAT_TOP;


    public static Integer OB_PULSE_VPT;

    public static int CENTER_NODE_INSTANCE;
    public static int GAME_NODE_INSTANCE;

    public static int GATE_NODE_INSTANCE;
    public static int GATE_NET_PORT;
    public static String GATE_NET_PATH;


    public static int LOGIC_NODE_INSTANCE;
    public static int DB_NODE_INSTANCE;


    public static void init() {
        // 获取配置
        Props props = new Props(CONFIG_PATH + CONFIG_NAME);

        COMMON_GAME = props.getStr("common.game");
        COMMON_DEBUG = props.getBool("common.debug");
        COMMON_FPS = props.getInt("common.fps");
        COMMON_DEPLOY = DeployConfig.valueOf(props.getStr("common.deploy"));

        CLUSTER_HOST = props.getStr("cluster.host");
        CLUSTER_MEMBER = props.getStr("cluster.member");

        STAT_ENABLE = props.getBool("stat.enable");
        STAT_TOP = props.getInt("stat.top");

        OB_PULSE_VPT = props.getInt("ob.pulse_vpt");

        CENTER_NODE_INSTANCE = props.getInt("center.node.instance");
        GAME_NODE_INSTANCE = props.getInt("game.node.instance");

        GATE_NODE_INSTANCE = props.getInt("gate.node.instance");
        GATE_NET_PORT = props.getInt("gate.net.port");
        GATE_NET_PATH = props.getStr("gate.net.path");

        LOGIC_NODE_INSTANCE = props.getInt("logic.node.instance");
        DB_NODE_INSTANCE = props.getInt("db.node.instance");
    }
}
