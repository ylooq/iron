package cn.pink.core.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.cp.SemaphoreConfig;

import java.util.Map;

/**
 * Hazelcast 集群管理器 配置
 * @Author: pink
 * @Date: 2022/5/20 14:39
 */
public class HazelcastConfig {

    public static Config getConfig() {
        Config config = new Config(IronConfig.COMMON_GAME);

        config.setProperty("hazelcast.logging.type", "slf4j");
        config.setProperty("hazelcast.connection.monitor.max.faults", "0");

        // 关闭分区
        config.getPartitionGroupConfig().setEnabled(false);

        // 关闭UDP组播，采用TCP进行集群通信。
        JoinConfig joinConfig = config.getNetworkConfig().getJoin();
        joinConfig.getMulticastConfig().setEnabled(false);
        joinConfig.getTcpIpConfig().setEnabled(true);
        joinConfig.getTcpIpConfig().addMember(IronConfig.CLUSTER_MEMBER);

        // 这里指定所用通信的网卡（在本机多个网卡时如不指定会有问题，无论有无多个网卡最好设置一下。）
        config.getNetworkConfig().getInterfaces().setEnabled(true);
        config.getNetworkConfig().getInterfaces().addInterface(IronConfig.CLUSTER_HOST);

        // 配置信号量  否则分布式锁不生效
        config.getCPSubsystemConfig().setSemaphoreConfigs(Map.of("__vertx.*", new SemaphoreConfig().setInitialPermits(1).setJDKCompatible(false)));

        return config;
    }
}
