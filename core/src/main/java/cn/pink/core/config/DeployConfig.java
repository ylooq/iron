package cn.pink.core.config;

/**
 * @Author: pink
 * @Date: 2022/5/19 16:49
 */
public enum DeployConfig {
    /** 单机部署 */
    SIMPLE,
    /** 集群部署 */
    CLUSTER,
    /** 分布式部署 */
    DISTRIBUTED;
}
