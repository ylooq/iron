package cn.pink.core;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * call返回值基类
 * @author Pink
 */
public abstract class CallResultBase {
	/** 对应的请求ID */
	private final long callId;
	/** 请求过期时间 */
	private final long timeout;
	
	/**
	 * 处理返回值
	 */
	public abstract void onResult(Call call);
	
	/**
	 * 等待返回值超时 进行后续处理
	 */
	public abstract void onTimeout();
	
	/**
	 * 构造函数
	 */
	public CallResultBase(long callId, long timeoutDelay) {
		this.callId = callId;
		this.timeout = System.currentTimeMillis() + timeoutDelay;
	}
	
	/**
	 * 是否超时
	 */
	public boolean isTimeout() {
		return timeout < System.currentTimeMillis();
	}
	
	/**
	 * 获取监听的CallId
	 */
	public long getCallId() {
		return callId;
	}
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("callId", getCallId()).toString();
	}
}
