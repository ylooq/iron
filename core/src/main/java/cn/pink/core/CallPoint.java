package cn.pink.core;

import java.io.Serializable;

/**
 * 传输点信息
 * @Author: pink
 * @Date: 2022/6/7 11:11
 */
public class CallPoint implements Serializable {
    public String nodeId;
    public String portId;
    public Object servId;
    /** 调用者信息 */
    public String callerInfo;

    public CallPoint() {}

    /**
     * 构造函数
     */
    public CallPoint(String nodeId, String portId, Object servId) {
        this.nodeId = nodeId;
        this.portId = portId;
        this.servId = servId;
        this.callerInfo = "";
    }
}
