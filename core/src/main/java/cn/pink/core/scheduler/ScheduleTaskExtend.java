package cn.pink.core.scheduler;

import org.quartz.JobKey;
import org.quartz.utils.Key;

/**
 * 时间调度任务
 * @author Pink
 */
public abstract class ScheduleTaskExtend extends ScheduleTask {
	private String jobName = null;
	private String jobGroup = null;
	
	@Override
	public String getJobName() {
		return jobName;
	}
	
	@Override
	public String getJobGroup() {
		return jobGroup;
	}
	
	public ScheduleTaskExtend() {
		this(null, null);
	}
	
	public ScheduleTaskExtend(String jobName) {
		this(jobName, null);
	}
	
	public ScheduleTaskExtend(String jobName, String JobGroup) {
		super();
		this.jobName = jobName;
		this.jobGroup = JobGroup;
		
		if (getJobName() == null) {
			jobKey = new JobKey(Key.createUniqueName(getJobGroup()), getJobGroup());
		} else {
			jobKey = JobKey.jobKey(getJobName(), getJobGroup());
		}
	}
	

}
