package cn.pink.core.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * quartz调度任务
 */
public class ScheduleJob implements Job {
	/** 任务 */
	private ScheduleTask task;
	/** 存储Service.schedulerList的引用 */
	private ConcurrentLinkedQueue<ScheduleTask> scheduler = new ConcurrentLinkedQueue<ScheduleTask>();
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		task.state = ScheduleTask.STATE_INQUEUE;
		
		// 到触发时间后放入时间调度队列
		scheduler.add(task);
	}

	public ScheduleTask getTask() {
		return task;
	}

	public void setTask(ScheduleTask task) {
		this.task = task;
	}

	public ConcurrentLinkedQueue<ScheduleTask> getScheduler() {
		return scheduler;
	}

	public void setScheduler(ConcurrentLinkedQueue<ScheduleTask> scheduler) {
		this.scheduler = scheduler;
	}
}
