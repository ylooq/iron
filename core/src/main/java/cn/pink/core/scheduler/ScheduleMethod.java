package cn.pink.core.scheduler;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Pink
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ScheduleMethod {
	
	/**
	 * 被调度的时间，quartz的cron表达式
	 */
	String[] value();
	
}
