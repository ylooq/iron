package cn.pink.core;

import lombok.Data;

import java.io.Serializable;

/**
 * 节点信息
 * @Author: pink
 * @Date: 2022/6/13 16:08
 */
@Data
public class NodeInfo implements Serializable {
    /** 节点id gate0 game1 ... */
    private String nodeId;

    /** portId */
    private String portId;

    /** 节点类型 */
    private NodeType nodeType;

    public NodeInfo(String nodeId, String portId, NodeType nodeType) {
        this.nodeId = nodeId;
        this.portId = portId;
        this.nodeType = nodeType;
    }
}
