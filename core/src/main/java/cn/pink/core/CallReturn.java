package cn.pink.core;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * 封装一个rpc返回
 * @author Pink
 */
public class CallReturn implements Serializable {
	/** 请求ID */
	public long id;
	/** 返回的nodeId */
	public String nodeId;
	/** 返回的portId */
	public String portId;
	
	public CallReturn() { }

	public CallReturn(long id, String nodeId, String portId) {
		this.id = id;
		this.nodeId = nodeId;
		this.portId = portId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
					.append("id", id)
					.append("nodeId", nodeId)
					.append("portId", portId)
					.toString();
	}
}
