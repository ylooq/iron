package cn.pink.core;


import cn.pink.core.support.Param;

/**
 * 异步任务，port的下次心跳执行
 * @author Pink
 */
public abstract class PortTask {
	/** 上下文环境 */
	public final Param param;
	
	/**
	 * 构造函数
	 */
	public PortTask() {
		this.param = new Param();
	}
	
	/**
	 * 构造函数
	 * 可传递上下文参数
	 */
	public PortTask(Object...params) {
		this.param = new Param(params);
	}
	
	/**
	 * 任务执行
	 */
	public abstract void execute(Port port);
}