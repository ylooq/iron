package cn.pink.logicsrv;

import cn.pink.core.gen.proxy.RPCProxyBase;
import cn.pink.core.*;
import cn.pink.common.support.tools.node.NodeTool;
import cn.pink.core.support.Param;
import cn.pink.core.support.Utils;
import cn.pink.core.support.function.*;
import cn.pink.core.gen.IronGenFile;


@IronGenFile
public class RPCProxy {
	public static final class LogicServiceProxy extends RPCProxyBase {
		public final class EnumCall{
			public static final int CN_PINK_LOGICSRV_LOGICSERVICE_DEFAULTHANDLER_STRING = 1;
		}

		public static final String DIST_NAME = "LogicService";

		private CallPoint remote;
		private Port localPort;

		private static NodeType nodeType = NodeType.LOGIC;
		
		/**
		 * 私有构造函数
		 * 防止实例被私自创建 必须通过newInstance函数
		 */
		private LogicServiceProxy() {}
	
		/**
		 * 获取实例
		 * 大多数情况下可用此函数获取
		 */
		public static LogicServiceProxy newInstance() {
			NodeInfo nodeInfo = NodeTool.getInstance().getRandomNode(nodeType);
			return createInstance(nodeInfo.getNodeId(), nodeInfo.getPortId(), DIST_NAME);
		}

		/**
		 * 创建实例
		 */
		private static LogicServiceProxy createInstance(String node, String port, Object id) {
			LogicServiceProxy inst = new LogicServiceProxy();
			inst.localPort = Port.getCurrent();
			inst.remote = new CallPoint(node, port, id);
			
			return inst;
		}
		
		/**
		 * 监听返回值
		 */
		public void listenResult(IronFunction2<Param, Param> method, Object...context) {
			listenResult(method, new Param(context));
		}
		
		/**
		 * 监听返回值
		 */
		public void listenResult(IronFunction2<Param, Param> method, Param context) {
			context.put("_callerInfo", remote.callerInfo);
			localPort.listenResult(method, context);
		}
		
		
		public void listenResult(IronFunction3<Boolean, Param, Param> method, Object...context) {
			listenResult(method, new Param(context));
		}
		
		public void listenResult(IronFunction3<Boolean, Param, Param> method, Param context) {
			context.put("_callerInfo", remote.callerInfo);
			localPort.listenResult(method, context);
		}
		
		public void defaultHandler(String arg0) {
			remote.callerInfo = Utils.getCallerInfo();
			localPort.call(remote, EnumCall.CN_PINK_LOGICSRV_LOGICSERVICE_DEFAULTHANDLER_STRING, new Object[]{ arg0 });
		}
		
		public Call makeCall_defaultHandler(String arg0) {
			Call call = localPort.makeCall(remote, EnumCall.CN_PINK_LOGICSRV_LOGICSERVICE_DEFAULTHANDLER_STRING, new Object[]{ arg0 });
			call.to.callerInfo = Utils.getCallerInfo();
			
			return call;
		}	
	}
	
}