package cn.pink.common.support.tools.cache;

import cn.pink.core.support.Log;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.Scheduler;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * @Author: pink
 * @Date: 2022/6/14 18:49
 */
public class CaffeineProvider implements ICacheProvider {
    private ConcurrentHashMap<String, ICache> caches = new ConcurrentHashMap<>();

    @Override
    public ICache buildCache(String region, long size, long ttl) {
        return caches.computeIfAbsent(region, v -> newCache(size, ttl));
    }

    private ICache newCache(long size, long ttl) {
        CaffeineCache cache = new CaffeineCache(size, ttl);

        Caffeine<Object, Object> caffeine = Caffeine.newBuilder();

        caffeine.scheduler(Scheduler.systemScheduler());
        caffeine.removalListener((k, v, cause) -> {
            if(cause == RemovalCause.EXPIRED) {
                cache.doRemove((String) k, v, RemoveReason.EXPIRED);
            }

            if(cause == RemovalCause.SIZE) {
                cache.doRemove((String) k, v, RemoveReason.SIZE);
            }

            if(cause == RemovalCause.EXPLICIT) {
                cache.doRemove((String) k, v, RemoveReason.EXPLICIT);
            }

            Log.system.info("remove cache, key={}, oldVal={}, reason={}", k, v, cause);
        });

        if(size > 0) {
            caffeine.maximumSize(size);
        }

        if (ttl > 0) {
            caffeine.expireAfterWrite(ttl, TimeUnit.SECONDS);
        }
        else {
            caffeine.expireAfterWrite(Long.MAX_VALUE, TimeUnit.DAYS);
        }

        cache.setCache(caffeine.build());

        return cache;
    }
}
