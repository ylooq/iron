package cn.pink.common.support.tools.cache;

/**
 * 缓存监听器
 * @Author: pink
 * @Date: 2022/6/10 18:33
 */
public interface ICacheListener {

    /**
     * 添加元素
     * @param key key
     * @param value value
     */
    void elementAdd(String key, Object value);

    /**
     * 更新元素
     * @param key key
     * @param value value
     * @param oldValue oldValue
     */
    void elementUpdate(String key, Object value, Object oldValue);

    /**
     * 删除元素
     * @param key key
     * @param oldValue oldValue
     * @param reason 删除原因
     */
    void elementRemove(String key, Object oldValue, RemoveReason reason);
}
