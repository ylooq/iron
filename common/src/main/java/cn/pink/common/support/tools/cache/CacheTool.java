package cn.pink.common.support.tools.cache;

import cn.pink.core.config.CacheConfig;
import cn.pink.core.config.DeployConfig;
import cn.pink.core.config.IronConfig;

/**
 * 缓存插件
 * @Author: pink
 * @Date: 2022/6/10 21:34
 */
public class CacheTool {
    private static CacheTool cacheTool = new CacheTool();

    private CacheTool() {

    }

    public static CacheTool getInstance() {
        return cacheTool;
    }

    private ICacheProvider provider;

    public void init() {
        if(IronConfig.COMMON_DEPLOY != DeployConfig.SIMPLE) {
            //除了单机部署 其余默认使用hazelcast
            provider = new HazelcastProvider();
        }
        else {
            provider = new CaffeineProvider();
        }
    }

    /**
     * 获取缓存
     */
    public ICache getCache(CacheConfig cacheConfig) {
        return provider.buildCache(cacheConfig.getKey(), cacheConfig.getSize(), cacheConfig.getTtl());
    }

    /**
     * 注册缓存监听
     */
    public void registerListener(CacheConfig cacheConfig, ICacheListener listener) {
        getCache(cacheConfig).registerListener(listener);
    }
}
