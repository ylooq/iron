package cn.pink.common.support.tools.cache;

/**
 * 缓存中元素移除原因
 * @Author: pink
 * @Date: 2022/6/10 18:37
 */
public enum RemoveReason {
    /** 手动显示删除 */
    EXPLICIT("explicit"),

    /** 过期删除 */
    EXPIRED("expired"),

    /** 超过容量 LRU */
    SIZE("size"),
    ;

    private final String value;

    RemoveReason(String value) {
        this.value = value;
    }
}
