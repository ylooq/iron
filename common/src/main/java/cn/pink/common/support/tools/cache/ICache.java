package cn.pink.common.support.tools.cache;

import java.util.Collection;
import java.util.Map;

/**
 * 缓存
 * @Author: pink
 * @Date: 2022/6/10 18:30
 */
public interface ICache {
    /**
     * get
     * @param key key
     * @return object
     */
    Object get(String key) ;

    /**
     * put
     * @param key key
     * @param value value
     */
    void put(String key, Object value);

    /**
     * 批量获取缓存对象
     * @param keys keys
     * @return return map
     */
    Map<String, Object> get(Collection<String> keys);

    /**
     * 判断缓存是否存在
     * @param key key
     * @return bool
     */
    default boolean exists(String key) {
        return get(key) != null;
    }

    /**
     * 批量插入数据
     * @param elements elements
     */
    void put(Map<String, Object> elements);

    /**
     * 返回键的集合
     * @return 返回键的集合
     */
    Collection<String> keys();

    /**
     * 返回值的集合
     * @return 返回值的集合
     */
    Collection<Object> values() ;

    /**
     * 移除一条缓存
     * @param key key
     */
    void remove(String key);

    /**
     * 清空缓存
     */
    void clear();

    /**
     * 返回该缓存区域的 TTL 设置（单位：秒）
     * @return ttl
     */
    long ttl();

    /**
     * 返回该缓存区域中，内存存储对象的最大数量, 超过将淘汰
     * @return size
     */
    long size();

    /**
     * 注册缓存监听
     * @param listener 监听器
     */
    void registerListener(ICacheListener listener);
}
