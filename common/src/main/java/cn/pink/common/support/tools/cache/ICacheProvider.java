package cn.pink.common.support.tools.cache;

/**
 * 缓存提供器
 * @Author: pink
 * @Date: 2022/6/10 18:45
 */
public interface ICacheProvider {
    /**
     * 构建缓存对象
     * @param region 缓存名称
     * @param size size
     * @param ttl ttl
     * @return ICache
     */
    ICache buildCache(String region, long size, long ttl);
}
