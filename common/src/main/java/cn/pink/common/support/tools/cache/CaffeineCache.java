package cn.pink.common.support.tools.cache;

import com.github.benmanes.caffeine.cache.Cache;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;

/**
 * Caffeine 本地缓存
 * @Author: pink
 * @Date: 2022/6/14 18:26
 */
public class CaffeineCache implements ICache{
    @Getter
    @Setter
    private Cache<String, Object> cache;

    private long size;
    private long expire;

    private ICacheListener listener;

    public CaffeineCache(long size, long expire) {
        this.size = size;
        this.expire = expire;
    }

    @Override
    public Object get(String key) {
        return cache.getIfPresent(key);
    }

    @Override
    public void put(String key, Object value) {
        Object oldValue = get(key);
        if(oldValue != null && listener != null) {
            listener.elementUpdate(key, value, oldValue);
        }

        cache.put(key, value);

        if(listener != null) {
            listener.elementAdd(key, value);
        }
    }

    @Override
    public Map<String, Object> get(Collection<String> keys) {
        return cache.getAllPresent(keys);
    }

    @Override
    public void put(Map<String, Object> elements) {
        cache.putAll(elements);
    }

    @Override
    public Collection<String> keys() {
        return cache.asMap().keySet();
    }

    @Override
    public Collection<Object> values() {
        return cache.asMap().values();
    }

    @Override
    public void remove(String key) {
        cache.invalidate(key);
    }

    @Override
    public void clear() {
        cache.invalidateAll();
    }

    @Override
    public long ttl() {
        return expire;
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public void registerListener(ICacheListener listener) {
        this.listener = listener;
    }

    /**
     * 执行移除监听
     */
    public void doRemove(String key, Object oldValue, RemoveReason reason) {
        if(listener == null) {
            return;
        }

        listener.elementRemove(key, oldValue, reason);
    }
}
