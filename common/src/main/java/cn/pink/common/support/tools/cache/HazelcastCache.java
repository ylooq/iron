package cn.pink.common.support.tools.cache;

import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;
import com.hazelcast.map.IMap;
import com.hazelcast.map.MapEvent;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * hazelcast 缓存
 * @Author: pink
 * @Date: 2022/6/10 18:50
 */
public class HazelcastCache implements ICache {
    @Getter
    @Setter
    private IMap<String, Object> cache;

    private long size;
    private long expire;

    public HazelcastCache(long size, long expire) {
        this.size = size;
        this.expire = expire;
    }

    @Override
    public Object get(String key) {
        return cache.get(key);
    }

    @Override
    public void put(String key, Object value) {
        if(expire > 0) {
            cache.put(key, value, expire, TimeUnit.SECONDS);
            return;
        }

        cache.put(key, value);
    }

    @Override
    public Map<String, Object> get(Collection<String> keys) {
        return null;
    }

    @Override
    public void put(Map<String, Object> elements) {

    }

    @Override
    public Collection<String> keys() {
        return cache.keySet();
    }

    @Override
    public Collection<Object> values() {
        return cache.values();
    }

    @Override
    public void remove(String key) {
        cache.remove(key);
    }

    @Override
    public void clear() {
        cache.clear();
    }

    @Override
    public long ttl() {
        return expire;
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public void registerListener(ICacheListener listener) {
        cache.addEntryListener(new EntryListener<String, Object>() {
            @Override
            public void mapEvicted(MapEvent event) {
                //驱逐所有元素
            }

            @Override
            public void mapCleared(MapEvent event) {
                //清空整个map, clear时调用
            }

            @Override
            public void entryUpdated(EntryEvent<String, Object> event) {
                listener.elementUpdate(event.getKey(), event.getValue(), event.getOldValue());
            }

            @Override
            public void entryRemoved(EntryEvent<String, Object> event) {
                listener.elementRemove(event.getKey(), event.getOldValue(), RemoveReason.EXPLICIT);
            }

            @Override
            public void entryExpired(EntryEvent<String, Object> event) {
                listener.elementRemove(event.getKey(), event.getOldValue(), RemoveReason.EXPIRED);
            }

            @Override
            public void entryEvicted(EntryEvent<String, Object> event) {
                listener.elementRemove(event.getKey(), event.getOldValue(), RemoveReason.SIZE);
            }

            @Override
            public void entryAdded(EntryEvent<String, Object> event) {
                listener.elementAdd(event.getKey(), event.getValue());
            }
        }, true);
    }
}
