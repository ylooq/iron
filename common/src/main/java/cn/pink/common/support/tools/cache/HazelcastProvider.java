package cn.pink.common.support.tools.cache;

import cn.pink.core.config.IronConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Hazelcast 缓存 提供器
 * @Author: pink
 * @Date: 2022/6/10 20:27
 */
public class HazelcastProvider implements ICacheProvider {
    private ConcurrentHashMap<String, ICache> caches = new ConcurrentHashMap<>();

    @Override
    public ICache buildCache(String region, long size, long ttl) {
        return caches.computeIfAbsent(region, v -> newCache(region, size, ttl));
    }

    private ICache newCache(String region, long size, long ttl) {
        HazelcastInstance instance = Hazelcast.getHazelcastInstanceByName(IronConfig.COMMON_GAME);

        HazelcastCache cache = new HazelcastCache(size, ttl);
        cache.setCache(instance.getMap(region));

        return cache;
    }
}
