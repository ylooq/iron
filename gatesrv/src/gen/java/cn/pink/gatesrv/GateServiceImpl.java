package cn.pink.gatesrv;
                    
import cn.pink.core.Service;
import cn.pink.core.gen.proxy.RPCImplBase;
import cn.pink.gatesrv.RPCProxy.GateServiceProxy.EnumCall;
import cn.pink.core.support.function.*;
import cn.pink.core.gen.IronGenFile;

@SuppressWarnings("unchecked")
@IronGenFile
public final class GateServiceImpl extends RPCImplBase {
	
	/**
	 * 获取函数指针
	 */
	@Override	
	public Object getMethodFunction(Service service, int methodKey) {
		GateService serv = (GateService)service;
		switch (methodKey) {
			case EnumCall.CN_PINK_GATESRV_GATESERVICE_DEFAULTHANDLER_STRING: {
				return (IronFunction1<String>)serv::defaultHandler;
			}
			default: break;
		}
		
		return null;
	}

}
