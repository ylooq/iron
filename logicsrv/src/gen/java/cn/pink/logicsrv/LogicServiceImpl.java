package cn.pink.logicsrv;
                    
import cn.pink.core.Service;
import cn.pink.core.gen.proxy.RPCImplBase;
import cn.pink.logicsrv.RPCProxy.LogicServiceProxy.EnumCall;
import cn.pink.core.support.function.*;
import cn.pink.core.gen.IronGenFile;

@SuppressWarnings("unchecked")
@IronGenFile
public final class LogicServiceImpl extends RPCImplBase {
	
	/**
	 * 获取函数指针
	 */
	@Override	
	public Object getMethodFunction(Service service, int methodKey) {
		LogicService serv = (LogicService)service;
		switch (methodKey) {
			case EnumCall.CN_PINK_LOGICSRV_LOGICSERVICE_DEFAULTHANDLER_STRING: {
				return (IronFunction1<String>)serv::defaultHandler;
			}
			default: break;
		}
		
		return null;
	}

}
