package cn.pink.logicsrv;

import cn.pink.core.NodeType;
import cn.pink.core.Port;
import cn.pink.core.Service;
import cn.pink.core.gen.proxy.DistrClass;
import cn.pink.core.gen.proxy.DistrMethod;

/**
 * @Author: pink
 * @Date: 2022/6/21 16:53
 */
@DistrClass(nodeType = NodeType.LOGIC)
public class LogicService extends Service {
    public LogicService(Port port) {
        super(port);
    }

    @Override
    public Object getId() {
        return RPCProxy.LogicServiceProxy.DIST_NAME;
    }

    @DistrMethod
    public void defaultHandler(String str) {
        System.out.println(str);
        port.returns("rererererererererererere");
    }
}
