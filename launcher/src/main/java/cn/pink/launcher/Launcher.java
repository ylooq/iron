package cn.pink.launcher;

import cn.pink.core.support.Log;
import cn.pink.common.support.tools.cache.CacheTool;
import cn.pink.common.support.tools.node.NodeTool;
import cn.pink.common.support.tools.vertx.VertxTool;
import cn.pink.core.Call;
import cn.pink.core.CallMessageCodec;
import io.vertx.core.Vertx;

/**
 * 启动器
 * @Author: pink
 * @Date: 2022/5/19 17:08
 */
public abstract class Launcher {
    /**
     * 启动
     */
    protected static void launcher(Vertx vertx) {
        vertx.eventBus().registerDefaultCodec(Call.class, new CallMessageCodec());

        //插件初始化
        CacheTool.getInstance().init();
        VertxTool.getInstance().init(vertx);
        NodeTool.getInstance().init();
    }

    protected static void afterLauncher(Vertx vertx) {
        NodeTool.getInstance().getAllNode().forEach(nodeInfo -> {
            Log.system.info("节点信息: nodeId={}, portId={}, nodeType={}", nodeInfo.getNodeId(), nodeInfo.getPortId(), nodeInfo.getNodeType());
        });
    }
}
