package cn.pink.launcher;

import cn.pink.centersrv.CenterNode;
import cn.pink.core.config.HazelcastConfig;
import cn.pink.core.config.IronConfig;
import cn.pink.core.config.NodeDeployConfig;
import cn.pink.core.support.Log;
import cn.pink.core.support.Utils;
import cn.pink.core.support.wrap.VInt;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

/**
 * @Author: pink
 * @Date: 2022/5/19 16:24
 */
public class CenterLauncher extends Launcher{
    public static void main(String[] args) {
        VInt index = new VInt();
        if(args.length > 0) {
            index.value = Utils.intValue(args[0]);
        }

        IronConfig.init();

        System.setProperty("java.net.preferIPv4Stack" , "true");
        System.setProperty("logFileName", "center");

        VertxOptions options = new VertxOptions().setClusterManager(new HazelcastClusterManager(HazelcastConfig.getConfig()));
        Vertx.clusteredVertx(options)
                .onSuccess(v -> {
                    launcher(v);
                    v.deployVerticle(CenterNode.class.getName(), NodeDeployConfig.getCenterOpt(index.value, IronConfig.CENTER_NODE_INSTANCE))
                            .onSuccess(r -> {
                                afterLauncher(v);
                                Log.system.info("中心服启动成功");
                            })
                            .onFailure(e -> {
                                Log.error.error(e.toString());
                            });
                })
                .onFailure(e -> {
                    Log.error.error(e.toString());
                });
    }
}
