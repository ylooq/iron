package cn.pink.launcher;

import cn.pink.core.config.HazelcastConfig;
import cn.pink.core.config.IronConfig;
import cn.pink.core.config.NodeDeployConfig;
import cn.pink.core.support.Log;
import cn.pink.core.support.Utils;
import cn.pink.core.support.wrap.VInt;
import cn.pink.gamesrv.GameNode;
import cn.pink.gatesrv.GateNode;
import cn.pink.logicsrv.LogicNode;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

/**
 * 集群启动
 * @author Pink
 */
public class ClusterLauncher extends Launcher{
    public static void main(String[] args) {
        VInt index = new VInt();
        if(args.length > 0) {
            index.value = Utils.intValue(args[0]);
        }

        IronConfig.init();

        System.setProperty("java.net.preferIPv4Stack" , "true");
        System.setProperty("logFileName", "cluster");

        VertxOptions options = new VertxOptions().setClusterManager(new HazelcastClusterManager(HazelcastConfig.getConfig()));
        Vertx.clusteredVertx(options)
                .onSuccess(v -> {
                    launcher(v);

                    v.deployVerticle(GameNode.class.getName(), NodeDeployConfig.getGameOpt(index.value, IronConfig.GAME_NODE_INSTANCE))
                            .compose(r -> v.deployVerticle(LogicNode.class.getName(), NodeDeployConfig.getLogicOpt(index.value, IronConfig.LOGIC_NODE_INSTANCE)))
                            .compose(r -> v.deployVerticle(GateNode.class.getName(), NodeDeployConfig.getGateOpt(index.value, IronConfig.GATE_NODE_INSTANCE)))
                            .onSuccess(r -> {
                                afterLauncher(v);
                                Log.system.info("全部服务启动完成");
                            })
                            .onFailure(e -> {
                                Log.error.error(e.toString());
                            });
                })
                .onFailure(e -> {
                    Log.error.error(e.toString());
                });
    }
}
