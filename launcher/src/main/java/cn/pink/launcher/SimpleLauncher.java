package cn.pink.launcher;

import cn.pink.centersrv.CenterNode;
import cn.pink.core.config.IronConfig;
import cn.pink.core.config.NodeDeployConfig;
import cn.pink.core.support.Log;
import cn.pink.core.support.Utils;
import cn.pink.core.support.wrap.VInt;
import cn.pink.gamesrv.GameNode;
import cn.pink.gatesrv.GateNode;
import cn.pink.logicsrv.LogicNode;
import io.vertx.core.Vertx;

/**
 * 简单模式启动器
 *
 * @Author: pink
 * @Date: 2022/5/18 17:13
 */
public class SimpleLauncher extends Launcher{
    public static void main(String[] args) {
        VInt index = new VInt();
        if(args.length > 0) {
            index.value = Utils.intValue(args[0]);
        }

        IronConfig.init();

        System.setProperty("logFileName", "simple");

        Vertx vertx = Vertx.vertx();

        launcher(vertx);

        vertx.deployVerticle(CenterNode.class.getName(), NodeDeployConfig.getCenterOpt(index.value, IronConfig.CENTER_NODE_INSTANCE))
                .compose(r -> vertx.deployVerticle(GameNode.class.getName(), NodeDeployConfig.getGameOpt(index.value, IronConfig.GAME_NODE_INSTANCE)))
                .compose(r -> vertx.deployVerticle(LogicNode.class.getName(), NodeDeployConfig.getLogicOpt(index.value, IronConfig.LOGIC_NODE_INSTANCE)))
                .compose(r -> vertx.deployVerticle(GateNode.class.getName(), NodeDeployConfig.getGateOpt(index.value, IronConfig.GATE_NODE_INSTANCE)))
                .onSuccess(r -> {
                    afterLauncher(vertx);
                    Log.system.info("全部服务启动完成");
                })
                .onFailure(e -> {
                    Log.error.error(e.toString());
                });
    }
}
