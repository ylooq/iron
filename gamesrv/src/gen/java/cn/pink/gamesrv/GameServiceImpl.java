package cn.pink.gamesrv;
                    
import cn.pink.core.Service;
import cn.pink.core.gen.proxy.RPCImplBase;
import cn.pink.gamesrv.RPCProxy.GameServiceProxy.EnumCall;
import cn.pink.core.support.function.*;
import cn.pink.core.gen.IronGenFile;

@SuppressWarnings("unchecked")
@IronGenFile
public final class GameServiceImpl extends RPCImplBase {
	
	/**
	 * 获取函数指针
	 */
	@Override	
	public Object getMethodFunction(Service service, int methodKey) {
		GameService serv = (GameService)service;
		switch (methodKey) {
			case EnumCall.CN_PINK_GAMESRV_GAMESERVICE_DEFAULTHANDLER_STRING: {
				return (IronFunction1<String>)serv::defaultHandler;
			}
			default: break;
		}
		
		return null;
	}

}
