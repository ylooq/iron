package cn.pink.gamesrv;

import cn.pink.core.Node;
import cn.pink.core.Port;
import cn.pink.core.NodeType;
import cn.pink.common.support.tools.node.NodeTool;
import io.vertx.core.Promise;

/**
 * 游戏服节点
 * @Author: pink
 * @Date: 2022/5/19 11:56
 */
public class GameNode extends Node {
    @Override
    public void start(Promise<Void> startPromise) {
        try {
            port = new Port(String.valueOf(Thread.currentThread().getId()));
            port.startup(this, false);

            port.addService(new GameService(port));

            NodeTool.getInstance().registerNode(getId(), port.getId(), NodeType.GAME).onSuccess(v -> super.start(startPromise));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        super.stop();
    }
}
