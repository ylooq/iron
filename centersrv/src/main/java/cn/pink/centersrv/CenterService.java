package cn.pink.centersrv;

import cn.pink.core.NodeType;
import cn.pink.core.Port;
import cn.pink.core.Service;
import cn.pink.core.gen.proxy.DistrClass;
import cn.pink.core.gen.proxy.DistrMethod;
import cn.pink.core.scheduler.ScheduleMethod;

/**
 * @Author: pink
 * @Date: 2022/6/17 15:52
 */
@DistrClass(nodeType = NodeType.CENTER)
public class CenterService extends Service {
    public CenterService(Port port) {
        super(port);
    }

    @Override
    public Object getId() {
        return RPCProxy.CenterServiceProxy.DIST_NAME;
    }

    @DistrMethod
    public void defaultHandler(String str) {
        System.out.println(str);
        port.returns("rererererererererererere");

        cn.pink.gamesrv.RPCProxy.GameServiceProxy proxy = cn.pink.gamesrv.RPCProxy.GameServiceProxy.newInstance();
        proxy.defaultHandler("ggoogogogogo");
    }

    /**
     * TODO 别忘了删
     */
    @ScheduleMethod("*/5 * * * * ?")
    public void testSche() {
//        System.out.println("55555555555555555555555");
    }
}
