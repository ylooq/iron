package cn.pink.centersrv;

import cn.pink.core.Node;
import cn.pink.core.Port;
import cn.pink.core.NodeType;
import cn.pink.common.support.tools.node.NodeTool;
import io.vertx.core.Promise;

/**
 * 中心服节点
 * @Author: pink
 * @Date: 2022/5/19 11:54
 */
public class CenterNode extends Node {
    @Override
    public void start(Promise<Void> startPromise) {
        try {
            port = new Port(String.valueOf(Thread.currentThread().getId()));
            port.startup(this, true);

            CenterService service = new CenterService(port);
            service.startup();
            port.addService(service);

            NodeTool.getInstance().registerNode(getId(), port.getId(), NodeType.CENTER).onSuccess(v -> super.start(startPromise));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() {
        super.stop();
    }
}
