package cn.pink.centersrv;
                    
import cn.pink.core.Service;
import cn.pink.core.gen.proxy.RPCImplBase;
import cn.pink.centersrv.RPCProxy.CenterServiceProxy.EnumCall;
import cn.pink.core.support.function.*;
import cn.pink.core.gen.IronGenFile;

@SuppressWarnings("unchecked")
@IronGenFile
public final class CenterServiceImpl extends RPCImplBase {
	
	/**
	 * 获取函数指针
	 */
	@Override	
	public Object getMethodFunction(Service service, int methodKey) {
		CenterService serv = (CenterService)service;
		switch (methodKey) {
			case EnumCall.CN_PINK_CENTERSRV_CENTERSERVICE_DEFAULTHANDLER_STRING: {
				return (IronFunction1<String>)serv::defaultHandler;
			}
			default: break;
		}
		
		return null;
	}

}
